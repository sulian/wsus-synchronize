# WSUS
 
Scripts d'administration d'un serveur WSUS
 
## Installation

Récupération des scripts depuis le service GitLab ou en clonant ce projet via `git@gitlab.exsymol.mc:others/wsus.git`
 
## Usage

Ouvrir un terminal PowerShell en tant qu'administrateur et exécuter le fichier `\mon_fichier.ps1`.
