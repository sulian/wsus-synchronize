<#
.SYNOPSIS
Synchronisation du serveur WSUS avec les serveurs de Microsoft

.DESCRIPTION
Script PowerShell permettant de synchroniser un serveur WSUS avec les serveurs
 de Microsoft

.EXAMPLE
.\synchronize-wsus.ps1

.LINK
http://gitlab.exsymol.mc/others/wsus/
https://4sysops.com/archives/automating-wsus-with-powershell/
#>

$file = "c:\WSUS\Synchronize_{0:MMddyyyy_HHmm}.log" -f (Get-Date)

<#
si le script est lanc� depuis le serveur WSUS, ce block r�soud un probl�me DNS
 vu que le serveur poss�de deux cartes r�seaux.
#>
if (($env:computername).ToUpper() -eq "VM-WS-04") {
    $wsusserver = "localhost"
} else {
    $wsusserver = "wu.exsymol.mc"
}

Start-Transcript -Path $file

[reflection.assembly]::LoadWithPartialName("Microsoft.UpdateServices.Administration") | out-null
$wsus = [Microsoft.UpdateServices.Administration.AdminProxy]::GetUpdateServer($wsusserver, $False,8530);
$wsus.GetSubscription().StartSynchronization();

Clear-Variable wsus

Stop-Transcript