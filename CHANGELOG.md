# Changelog

Tous les changements notables sur ce projet seront documentés dans ce fichier.

Le format est basé sur [Keep a Changelog](http://keepachangelog.com/fr/1.0.0/)
et suis les recommandations du [Versionnage Sémantique](http://semver.org/lang/fr/spec/v2.0.0.html).

## [Non publié]

---

## [0.1.1] - 2020-06-30

### Changed
- Script de synchronisation : utilisation en local

## [0.1.0] - 2020-06-30

### Added
- Script de synchronisation du serveur WSUS avec les serveurs de Microsoft
